#!/bin/bash
ROOT_UID=0
SUCCESS=0


if [ "$UID" -ne "$ROOT_UID" ]
							then
							  echo "Ejecutar como root este script"
							  echo "Formato-> sudo ./name_file"
							  exit $E_NOTROOT
							fi  

file=$1

if [ "${file}X" = "X" ];
						then
						echo "Pasar como parametro Usuarios.csv"
						echo "Formato-> sudo ./name_file Usuarios.csv"
						exit 1
						fi

eliminarUsuario(){

					userdel "${f1}"
					if [ $? -eq $SUCCESS ];
					then
						echo "Usuario [${f1}] eliminado exitosamente"
					else
						echo "Usuario [${f1}] no se pudo eliminar"
					fi
				}

while IFS=: read f1 f2 f3 f4 f5 f6 f7
do
	eliminarUsuario "\${f1}"	
done < ${file}

exit 0