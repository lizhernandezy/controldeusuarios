#!/bin/bash
ROOT_UID=0
SUCCESS=0

if [ "$UID" -ne "$ROOT_UID" ]
								then
								  echo "Ejecutar como root este script"
								  echo "Formato-> sudo ./name_file Grupos.csv"
								  exit $E_NOTROOT
								fi  

file=$1

if [ "${file}X" = "X" ];
						then
						   echo "Pasar como parametro Grupos.csv"
						   echo "Formato-> sudo ./name_file Grupos.csv"
						   exit 1
						fi

# Del archivo con el listado de usuarios a eliminar:
# Este es el formato:
# ejemplo
#    |   
#    f1  
#$f1 = username

eliminarGrupo(){

	eval user="$1"
	groupdel "${user}"
	if [ $? -eq $SUCCESS ];
							then
								echo "Usuario -> ${user} eliminado correctamente..."
							else
								echo "Usuario -> ${user} no se pudo eliminar..."
							fi
}

while IFS=: read -r f1
do
	eliminarGrupo "\${f1}"	
done < ${file}

exit 0