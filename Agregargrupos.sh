#!/bin/bash

ROOT_UID=0
SUCCESS=0

if [ "$UID" -ne "$ROOT_UID" ]
								then
								  echo "Ejecutar como root este script"
								  echo "Formato-> sudo ./name_file Grupos.csv"
								  exit $E_NOTROOT
								fi  

file=$1

if [ "${file}X" = "X" ];
							then
							   echo "Pasar como parametro Grupos.csv"
							   echo "Formato-> sudo ./name_file Grupos.csv"
							   exit 1
							fi



crearGrupo(){

				eval nombreGrupo="$1"
				groupadd "${nombreGrupo}"
	            if [ $? -eq $SUCCESS ];
										then
											echo "Grupo -> ${nombreGrupo} agregado correctamente."
										else
											echo "Grupo -> ${nombreGrupo} no se agreg�."
										fi
			}

while IFS=: read -r f1
						do
							crearGrupo "\${f1}"	
						done < ${file}

exit 0